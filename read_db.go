package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

// lastTableComment is used to keep track
// of update on database source table.
var lastTableComment string

// ConnectStr create string to connect to database
func ConnectStr() string {
	otherParams := "connect_timeout=5"
	return fmt.Sprintf(
		"user=%s dbname=%s password='%s' host=%s port=%s sslmode=%s  %s",
		SETTINGS.Get("PGUSER"),
		SETTINGS.Get("PGDATABASE"),
		SETTINGS.Get("PGPASSWORD"),
		SETTINGS.Get("PGHOST"),
		SETTINGS.Get("PGPORT"),
		SETTINGS.Get("PGSSLMODE"),
		otherParams,
	)
}

func dbConnect(connStr string) (*sql.DB, error) {
	//connStr := connectStr()
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return db, err
	}

	err = db.Ping()
	return db, err
}

type ItemInSQL struct {
	Pid                  sql.NullString
	Vid                  sql.NullString
	Sid                  sql.NullString
	Lid                  sql.NullString
	Numid                sql.NullString
	Huisnummer           sql.NullString
	Huisletter           sql.NullString
	Huisnummertoevoeging sql.NullString
	Postcode             sql.NullString
	Oppervlakte          sql.NullString
	Lidid                sql.NullString
	MatchScore           sql.NullString
	BagToevoeging        sql.NullString
	Slug                 sql.NullString
	Url                  sql.NullString
	Name                 sql.NullString
	BovagPlaats          sql.NullString
	BovagPostcode        sql.NullString
	BovagAdres           sql.NullString
	Geometry             sql.NullString
}

type Comment struct {
	Comment sql.NullString
}

/*
 * source database table should
 * have comment if comment is
 * changed we reload data.
 */
func tableComment() string {
	db, err := dbConnect(ConnectStr())

	if err != nil {
		log.Println(err)
		return ""
	}

	defer db.Close()

	query := `
	select obj_description('bovag.garages_lambda'::regclass);
	`

	rows, err := db.Query(query)

	if err != nil {
		log.Println(err)
		return ""
	}

	c := Comment{}

	for rows.Next() {
		if err := rows.Scan(&(c).Comment); err != nil {
			// Check for a scan error.
			// Query rows will be closed with defer.
			log.Println(err)
			return ""
		}
	}
	// fmt.Println(c.Comment)
	return convertSqlNullString(c.Comment)
}

// Every x seconds check database for changed comment.
// if changed replace data in ITEMS
func CheckForChange() {

	uptimeTicker := time.NewTicker(35 * time.Second)
	var newComment string

	for range uptimeTicker.C {
		newComment = tableComment()
		if newComment != lastTableComment {
			if newComment != "" {
				reset()
				fillFromDB(itemChan)
			}
		}
	}
}

func fillFromDB(items ItemsChannel) {
	// update last know comment from source table
	// so we can detect changes.
	lastTableComment = tableComment()

	db, err := dbConnect(ConnectStr())
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	query := `
	SELECT
	    pid,
	    vid,
	    sid,
	    lid,
	    numid,
	    huisnummer,
	    huisletter,
	    huisnummertoevoeging,
	    postcode,
	    oppervlakte,
            lidid,
            match_score,
            bag_toevoeging,
	    slug,
            url,
            "name",
            bovag_plaats,
            bovag_postcode,
            bovag_adres,
	    st_astext(st_transform(geometry, 4326)) as geometry
	FROM bovag.garages_lambda;
	`
	rows, err := db.Query(query)

	if err != nil {
		log.Fatal(err)
	}

	itemsArray := ItemsIn{}
	in := ItemInSQL{}

	rowCounter := 0

	for rows.Next() {
		if err := rows.Scan(
			&(in).Pid,
			&(in).Vid,
			&(in).Sid,
			&(in).Lid,
			&(in).Numid,
			&(in).Huisnummer,
			&(in).Huisletter,
			&(in).Huisnummertoevoeging,
			&(in).Postcode,
			&(in).Oppervlakte,
			&(in).Lidid,
			&(in).MatchScore,
			&(in).BagToevoeging,
			&(in).Slug,
			&(in).Url,
			&(in).Name,
			&(in).BovagPlaats,
			&(in).BovagPostcode,
			&(in).BovagAdres,
			&(in).Geometry,
		); err != nil {
			// Check for a scan error.
			// Query rows will be closed with defer.
			log.Fatal(err)
		}

		itemIn := &ItemIn{
			convertSqlNullString(in.Pid),
			convertSqlNullString(in.Vid),
			convertSqlNullString(in.Sid),
			convertSqlNullString(in.Lid),
			convertSqlNullString(in.Numid),
			convertSqlNullString(in.Huisnummer),
			convertSqlNullString(in.Huisletter),
			convertSqlNullString(in.Huisnummertoevoeging),
			convertSqlNullString(in.Postcode),
			convertSqlNullString(in.Oppervlakte),
			convertSqlNullString(in.Lidid),
			convertSqlNullString(in.MatchScore),
			convertSqlNullString(in.BagToevoeging),
			convertSqlNullString(in.Slug),
			convertSqlNullString(in.Url),
			convertSqlNullString(in.Name),
			convertSqlNullString(in.BovagPlaats),
			convertSqlNullString(in.BovagPostcode),
			convertSqlNullString(in.BovagAdres),
			convertSqlNullString(in.Geometry),
		}

		if len(itemsArray) > 1000 {
			itemChan <- itemsArray
			itemsArray = ItemsIn{}
		}

		itemsArray = append(itemsArray, itemIn)
		rowCounter += 1
	}

	// add left over items
	itemChan <- itemsArray

	// If the database is being written to ensure to check for Close
	// errors that may be returned from the driver. The query may
	// encounter an auto-commit error and be forced to rollback changes.
	rerr := rows.Close()

	if rerr != nil {
		log.Fatal(err)
	}
	// Rows.Err will report the last error encountered by Rows.Scan.
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	lock.Lock()
	defer lock.Unlock()
	S2CELLS.Sort()
	log.Printf("added %d rows from database", rowCounter)
}

func convertSqlNullString(v sql.NullString) string {
	if v.Valid {
		return v.String
	} else {
		return ""
	}
}

/*
func convertSqlNullInt(v sql.NullInt64) int64 {
	var err error
	var output []byte

	if v.Valid {
		output, err = json.Marshal(v.Int64)
	} else {
		output, err = json.Marshal(nil)
		return int64(0)
	}

	if err != nil {
		panic(err)
	}

	bla, err := strconv.ParseInt(string(output), 10, 64)

	if err != nil {
		panic(err)
	}

	return bla

}

func convertSqlNullFloat(v sql.NullFloat64) float64 {
	var err error
	var output []byte

	if v.Valid {
		output, err = json.Marshal(v.Float64)
	} else {
		output, err = json.Marshal(nil)
		return float64(0)
	}

	if err != nil {
		panic(err)
	}

	bla, err := strconv.ParseFloat(string(output), 64)

	if err != nil {
		panic(err)
	}

	return bla

}
*/
