module lambdadb

go 1.15

require (
	github.com/JensRantil/go-csv v0.0.0-20220328210211-99df005b4265
	github.com/Workiva/go-datastructures v1.1.5
	github.com/cheggaaa/pb v1.0.29
	github.com/go-spatial/geom v0.0.0-20220918193402-3cd2f5a9a082
	github.com/golang/geo v0.0.0-20230421003525-6adc56603217
	github.com/klauspost/compress v1.17.8 // indirect
	github.com/klauspost/pgzip v1.2.6
	github.com/kr/pretty v0.3.1 // indirect
	github.com/lib/pq v1.10.9
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/rogpeppe/go-internal v1.10.0 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	golang.org/x/sys v0.21.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
