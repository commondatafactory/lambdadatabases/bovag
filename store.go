package main

import (
	"fmt"
	"log"
	"runtime"
	"sync"
	"time"
)

// Global Items types
type Items []*Item
type ItemsIn []*ItemIn
type ItemsOut []*ItemOut

type ItemsGroupedBy map[string]Items
type ItemsChannel chan ItemsIn

// Global ITEMS storage.
var ITEMS Items

var itemChan ItemsChannel

// single item map lock when updating new items
var lock = sync.RWMutex{}
var label = 0

func init() {
	ITEMS = Items{}
}

// reset internal storage of items
// block api access
// clear indexes
// reset label counter
func reset() {

	lock.Lock()
	defer lock.Unlock()

	clearGeoIndex()
	clearBitArrays()

	ITEMS = Items{}
	label = 0

	go func() {
		time.Sleep(1 * time.Second)
		runtime.GC()
	}()

}

func ItemChanWorker(itemChan ItemsChannel) {
	//label := 0
	for items := range itemChan {
		lock.Lock()
		for _, itm := range items {
			if itm != nil {
				smallItem := itm.Shrink(label)
				smallItem.StoreBitArrayColumns()
				ITEMS = append(ITEMS, &smallItem)
				if ITEMS[label] != &smallItem {
					log.Fatal("storing item index off")
				}
				err := smallItem.GeoIndex(label)
				if err != nil {
					log.Printf("geo index error %v %v %v \n", smallItem, smallItem.GetGeometry(), err)
				}
				label++
			}
		}
		lock.Unlock()
	}
}

// After loading from byte, bytez storage indexed
// need to be rebuild.
func (items *Items) FillIndexes() {

	start := time.Now()

	lock.Lock()
	defer lock.Unlock()

	clearGeoIndex()
	clearBitArrays()

	for i := range *items {
		ITEMS[i].StoreBitArrayColumns()
		ITEMS[i].GeoIndex(ITEMS[i].Label)
	}

	S2CELLS.Sort()

	diff := time.Since(start)
	msg := fmt.Sprint("Index set time: ", diff)
	fmt.Printf(WarningColorN, msg)
	// run garbadge collection
	runtime.GC()
}
