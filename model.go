/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

	This codebase solves: I need to have an API on this
	tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"errors"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Pid                  string `json:"pid"`
	Vid                  string `json:"vid"`
	Sid                  string `json:"sid"`
	Lid                  string `json:"lid"`
	Numid                string `json:"numid"`
	Huisnummer           string `json:"huisnummer"`
	Huisletter           string `json:"huisletter"`
	Huisnummertoevoeging string `json:"huisnummertoevoeging"`
	Postcode             string `json:"postcode"`
	Oppervlakte          string `json:"oppervlakte"`
	Lidid                string `json:"lidid"`
	MatchScore           string `json:"match_score"`
	BagToevoeging        string `json:"bag_toevoeging"`
	Slug                 string `json:"slug"`
	Url                  string `json:"url"`
	Name                 string `json:"name"`
	BovagPlaats          string `json:"bovag_plaats"`
	BovagPostcode        string `json:"bovag_postcode"`
	BovagAdres           string `json:"bovag_adres"`
	Geometry             string `json:"geometry"`
}

type ItemOut struct {
	Pid                  string `json:"pid"`
	Vid                  string `json:"vid"`
	Sid                  string `json:"sid"`
	Lid                  string `json:"lid"`
	Numid                string `json:"numid"`
	Huisnummer           string `json:"huisnummer"`
	Huisletter           string `json:"huisletter"`
	Huisnummertoevoeging string `json:"huisnummertoevoeging"`
	Postcode             string `json:"postcode"`
	Oppervlakte          string `json:"oppervlakte"`
	Lidid                string `json:"lidid"`
	MatchScore           string `json:"match_score"`
	BagToevoeging        string `json:"bag_toevoeging"`
	Slug                 string `json:"slug"`
	Url                  string `json:"url"`
	Name                 string `json:"name"`
	BovagPlaats          string `json:"bovag_plaats"`
	BovagPostcode        string `json:"bovag_postcode"`
	BovagAdres           string `json:"bovag_adres"`
	Geometry             string `json:"geometry"`
}

type Item struct {
	Label                int // internal index in ITEMS
	Pid                  string
	Vid                  string
	Sid                  string
	Lid                  string
	Numid                string
	Huisnummer           string
	Huisletter           string
	Huisnummertoevoeging string
	Postcode             string
	Oppervlakte          string
	Lidid                string
	MatchScore           string
	BagToevoeging        string
	Slug                 string
	Url                  string
	Name                 string
	BovagPlaats          string
	BovagPostcode        string
	BovagAdres           string
	Geometry             string
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	return Item{

		label,

		i.Pid,
		i.Vid,
		i.Sid,
		i.Lid,
		i.Numid,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.Postcode,
		i.Oppervlakte,
		i.Lidid,
		i.MatchScore,
		i.BagToevoeging,
		i.Slug,
		i.Url,
		i.Name,
		i.BovagPlaats,
		i.BovagPostcode,
		i.BovagAdres,
		i.Geometry,
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast item selection
func (i *Item) StoreBitArrayColumns() {

}

func (i Item) Serialize() ItemOut {
	return ItemOut{

		i.Pid,
		i.Vid,
		i.Sid,
		i.Lid,
		i.Numid,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.Postcode,
		i.Oppervlakte,
		i.Lidid,
		i.MatchScore,
		i.BagToevoeging,
		i.Slug,
		i.Url,
		i.Name,
		i.BovagPlaats,
		i.BovagPostcode,
		i.BovagAdres,
		i.Geometry,
	}
}

func (i ItemIn) Columns() []string {
	return []string{

		"pid",
		"vid",
		"sid",
		"lid",
		"numid",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"postcode",
		"oppervlakte",
		"lidid",
		"match_score",
		"bag_toevoeging",
		"slug",
		"url",
		"name",
		"bovag_plaats",
		"bovag_postcode",
		"bovag_adres",
		"geometry",
	}
}

func (i ItemOut) Columns() []string {
	return []string{

		"pid",
		"vid",
		"sid",
		"lid",
		"numid",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"postcode",
		"oppervlakte",
		"lidid",
		"match_score",
		"bag_toevoeging",
		"slug",
		"url",
		"name",
		"bovag_plaats",
		"bovag_postcode",
		"bovag_adres",
		"geometry",
	}
}

func (i Item) Row() []string {

	return []string{

		i.Pid,
		i.Vid,
		i.Sid,
		i.Lid,
		i.Numid,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.Postcode,
		i.Oppervlakte,
		i.Lidid,
		i.MatchScore,
		i.BagToevoeging,
		i.Slug,
		i.Url,
		i.Name,
		i.BovagPlaats,
		i.BovagPostcode,
		i.BovagAdres,
		i.Geometry,
	}
}

func (i Item) GetIndex() string {
	return GettersPid(&i)
}

func (i Item) GetGeometry() string {
	return GettersGeometry(&i)
}

// contain filter Pid
func FilterPidContains(i *Item, s string) bool {
	return strings.Contains(i.Pid, s)
}

// startswith filter Pid
func FilterPidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Pid, s)
}

// match filters Pid
func FilterPidMatch(i *Item, s string) bool {
	return i.Pid == s
}

// getter Pid
func GettersPid(i *Item) string {
	return i.Pid
}

// contain filter Vid
func FilterVidContains(i *Item, s string) bool {
	return strings.Contains(i.Vid, s)
}

// startswith filter Vid
func FilterVidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Vid, s)
}

// match filters Vid
func FilterVidMatch(i *Item, s string) bool {
	return i.Vid == s
}

// getter Vid
func GettersVid(i *Item) string {
	return i.Vid
}

// contain filter Sid
func FilterSidContains(i *Item, s string) bool {
	return strings.Contains(i.Sid, s)
}

// startswith filter Sid
func FilterSidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Sid, s)
}

// match filters Sid
func FilterSidMatch(i *Item, s string) bool {
	return i.Sid == s
}

// getter Sid
func GettersSid(i *Item) string {
	return i.Sid
}

// contain filter Lid
func FilterLidContains(i *Item, s string) bool {
	return strings.Contains(i.Lid, s)
}

// startswith filter Lid
func FilterLidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Lid, s)
}

// match filters Lid
func FilterLidMatch(i *Item, s string) bool {
	return i.Lid == s
}

// getter Lid
func GettersLid(i *Item) string {
	return i.Lid
}

// contain filter Numid
func FilterNumidContains(i *Item, s string) bool {
	return strings.Contains(i.Numid, s)
}

// startswith filter Numid
func FilterNumidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Numid, s)
}

// match filters Numid
func FilterNumidMatch(i *Item, s string) bool {
	return i.Numid == s
}

// getter Numid
func GettersNumid(i *Item) string {
	return i.Numid
}

// contain filter Huisnummer
func FilterHuisnummerContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnummer, s)
}

// startswith filter Huisnummer
func FilterHuisnummerStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnummer, s)
}

// match filters Huisnummer
func FilterHuisnummerMatch(i *Item, s string) bool {
	return i.Huisnummer == s
}

// getter Huisnummer
func GettersHuisnummer(i *Item) string {
	return i.Huisnummer
}

// contain filter Huisletter
func FilterHuisletterContains(i *Item, s string) bool {
	return strings.Contains(i.Huisletter, s)
}

// startswith filter Huisletter
func FilterHuisletterStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisletter, s)
}

// match filters Huisletter
func FilterHuisletterMatch(i *Item, s string) bool {
	return i.Huisletter == s
}

// getter Huisletter
func GettersHuisletter(i *Item) string {
	return i.Huisletter
}

// contain filter Huisnummertoevoeging
func FilterHuisnummertoevoegingContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnummertoevoeging, s)
}

// startswith filter Huisnummertoevoeging
func FilterHuisnummertoevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnummertoevoeging, s)
}

// match filters Huisnummertoevoeging
func FilterHuisnummertoevoegingMatch(i *Item, s string) bool {
	return i.Huisnummertoevoeging == s
}

// getter Huisnummertoevoeging
func GettersHuisnummertoevoeging(i *Item) string {
	return i.Huisnummertoevoeging
}

// contain filter Postcode
func FilterPostcodeContains(i *Item, s string) bool {
	return strings.Contains(i.Postcode, s)
}

// startswith filter Postcode
func FilterPostcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Postcode, s)
}

// match filters Postcode
func FilterPostcodeMatch(i *Item, s string) bool {
	return i.Postcode == s
}

// getter Postcode
func GettersPostcode(i *Item) string {
	return i.Postcode
}

// contain filter Oppervlakte
func FilterOppervlakteContains(i *Item, s string) bool {
	return strings.Contains(i.Oppervlakte, s)
}

// startswith filter Oppervlakte
func FilterOppervlakteStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Oppervlakte, s)
}

// match filters Oppervlakte
func FilterOppervlakteMatch(i *Item, s string) bool {
	return i.Oppervlakte == s
}

// getter Oppervlakte
func GettersOppervlakte(i *Item) string {
	return i.Oppervlakte
}

// contain filter Lidid
func FilterLididContains(i *Item, s string) bool {
	return strings.Contains(i.Lidid, s)
}

// startswith filter Lidid
func FilterLididStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Lidid, s)
}

// match filters Lidid
func FilterLididMatch(i *Item, s string) bool {
	return i.Lidid == s
}

// getter Lidid
func GettersLidid(i *Item) string {
	return i.Lidid
}

// contain filter MatchScore
func FilterMatchScoreContains(i *Item, s string) bool {
	return strings.Contains(i.MatchScore, s)
}

// startswith filter MatchScore
func FilterMatchScoreStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.MatchScore, s)
}

// match filters MatchScore
func FilterMatchScoreMatch(i *Item, s string) bool {
	return i.MatchScore == s
}

// getter MatchScore
func GettersMatchScore(i *Item) string {
	return i.MatchScore
}

// contain filter BagToevoeging
func FilterBagToevoegingContains(i *Item, s string) bool {
	return strings.Contains(i.BagToevoeging, s)
}

// startswith filter BagToevoeging
func FilterBagToevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.BagToevoeging, s)
}

// match filters BagToevoeging
func FilterBagToevoegingMatch(i *Item, s string) bool {
	return i.BagToevoeging == s
}

// getter BagToevoeging
func GettersBagToevoeging(i *Item) string {
	return i.BagToevoeging
}

// contain filter Slug
func FilterSlugContains(i *Item, s string) bool {
	return strings.Contains(i.Slug, s)
}

// startswith filter Slug
func FilterSlugStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Slug, s)
}

// match filters Slug
func FilterSlugMatch(i *Item, s string) bool {
	return i.Slug == s
}

// getter Slug
func GettersSlug(i *Item) string {
	return i.Slug
}

// contain filter Url
func FilterUrlContains(i *Item, s string) bool {
	return strings.Contains(i.Url, s)
}

// startswith filter Url
func FilterUrlStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Url, s)
}

// match filters Url
func FilterUrlMatch(i *Item, s string) bool {
	return i.Url == s
}

// getter Url
func GettersUrl(i *Item) string {
	return i.Url
}

// contain filter Name
func FilterNameContains(i *Item, s string) bool {
	return strings.Contains(i.Name, s)
}

// startswith filter Name
func FilterNameStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Name, s)
}

// match filters Name
func FilterNameMatch(i *Item, s string) bool {
	return i.Name == s
}

// getter Name
func GettersName(i *Item) string {
	return i.Name
}

// contain filter BovagPlaats
func FilterBovagPlaatsContains(i *Item, s string) bool {
	return strings.Contains(i.BovagPlaats, s)
}

// startswith filter BovagPlaats
func FilterBovagPlaatsStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.BovagPlaats, s)
}

// match filters BovagPlaats
func FilterBovagPlaatsMatch(i *Item, s string) bool {
	return i.BovagPlaats == s
}

// getter BovagPlaats
func GettersBovagPlaats(i *Item) string {
	return i.BovagPlaats
}

// contain filter BovagPostcode
func FilterBovagPostcodeContains(i *Item, s string) bool {
	return strings.Contains(i.BovagPostcode, s)
}

// startswith filter BovagPostcode
func FilterBovagPostcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.BovagPostcode, s)
}

// match filters BovagPostcode
func FilterBovagPostcodeMatch(i *Item, s string) bool {
	return i.BovagPostcode == s
}

// getter BovagPostcode
func GettersBovagPostcode(i *Item) string {
	return i.BovagPostcode
}

// contain filter BovagAdres
func FilterBovagAdresContains(i *Item, s string) bool {
	return strings.Contains(i.BovagAdres, s)
}

// startswith filter BovagAdres
func FilterBovagAdresStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.BovagAdres, s)
}

// match filters BovagAdres
func FilterBovagAdresMatch(i *Item, s string) bool {
	return i.BovagAdres == s
}

// getter BovagAdres
func GettersBovagAdres(i *Item) string {
	return i.BovagAdres
}

// contain filter Geometry
func FilterGeometryContains(i *Item, s string) bool {
	return strings.Contains(i.Geometry, s)
}

// startswith filter Geometry
func FilterGeometryStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Geometry, s)
}

// match filters Geometry
func FilterGeometryMatch(i *Item, s string) bool {
	return i.Geometry == s
}

// getter Geometry
func GettersGeometry(i *Item) string {
	return i.Geometry
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() error {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			if _, ok := RegisterFuncMap[f+"-"+c]; !ok {
				return errors.New(c + " is missing in RegisterMap")
			}
		}
	}
	return nil
}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	//RegisterFuncMap["search"] = 'EDITYOURSELF'
	// example RegisterFuncMap["search"] = FilterEkeyStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Pid
	RegisterFuncMap["match-pid"] = FilterPidMatch
	RegisterFuncMap["contains-pid"] = FilterPidContains
	RegisterFuncMap["startswith-pid"] = FilterPidStartsWith
	RegisterGetters["pid"] = GettersPid
	RegisterGroupBy["pid"] = GettersPid

	//register filters for Vid
	RegisterFuncMap["match-vid"] = FilterVidMatch
	RegisterFuncMap["contains-vid"] = FilterVidContains
	RegisterFuncMap["startswith-vid"] = FilterVidStartsWith
	RegisterGetters["vid"] = GettersVid
	RegisterGroupBy["vid"] = GettersVid

	//register filters for Sid
	RegisterFuncMap["match-sid"] = FilterSidMatch
	RegisterFuncMap["contains-sid"] = FilterSidContains
	RegisterFuncMap["startswith-sid"] = FilterSidStartsWith
	RegisterGetters["sid"] = GettersSid
	RegisterGroupBy["sid"] = GettersSid

	//register filters for Lid
	RegisterFuncMap["match-lid"] = FilterLidMatch
	RegisterFuncMap["contains-lid"] = FilterLidContains
	RegisterFuncMap["startswith-lid"] = FilterLidStartsWith
	RegisterGetters["lid"] = GettersLid
	RegisterGroupBy["lid"] = GettersLid

	//register filters for Numid
	RegisterFuncMap["match-numid"] = FilterNumidMatch
	RegisterFuncMap["contains-numid"] = FilterNumidContains
	RegisterFuncMap["startswith-numid"] = FilterNumidStartsWith
	RegisterGetters["numid"] = GettersNumid
	RegisterGroupBy["numid"] = GettersNumid

	//register filters for Huisnummer
	RegisterFuncMap["match-huisnummer"] = FilterHuisnummerMatch
	RegisterFuncMap["contains-huisnummer"] = FilterHuisnummerContains
	RegisterFuncMap["startswith-huisnummer"] = FilterHuisnummerStartsWith
	RegisterGetters["huisnummer"] = GettersHuisnummer
	RegisterGroupBy["huisnummer"] = GettersHuisnummer

	//register filters for Huisletter
	RegisterFuncMap["match-huisletter"] = FilterHuisletterMatch
	RegisterFuncMap["contains-huisletter"] = FilterHuisletterContains
	RegisterFuncMap["startswith-huisletter"] = FilterHuisletterStartsWith
	RegisterGetters["huisletter"] = GettersHuisletter
	RegisterGroupBy["huisletter"] = GettersHuisletter

	//register filters for Huisnummertoevoeging
	RegisterFuncMap["match-huisnummertoevoeging"] = FilterHuisnummertoevoegingMatch
	RegisterFuncMap["contains-huisnummertoevoeging"] = FilterHuisnummertoevoegingContains
	RegisterFuncMap["startswith-huisnummertoevoeging"] = FilterHuisnummertoevoegingStartsWith
	RegisterGetters["huisnummertoevoeging"] = GettersHuisnummertoevoeging
	RegisterGroupBy["huisnummertoevoeging"] = GettersHuisnummertoevoeging

	//register filters for Postcode
	RegisterFuncMap["match-postcode"] = FilterPostcodeMatch
	RegisterFuncMap["contains-postcode"] = FilterPostcodeContains
	RegisterFuncMap["startswith-postcode"] = FilterPostcodeStartsWith
	RegisterGetters["postcode"] = GettersPostcode
	RegisterGroupBy["postcode"] = GettersPostcode

	//register filters for Oppervlakte
	RegisterFuncMap["match-oppervlakte"] = FilterOppervlakteMatch
	RegisterFuncMap["contains-oppervlakte"] = FilterOppervlakteContains
	RegisterFuncMap["startswith-oppervlakte"] = FilterOppervlakteStartsWith
	RegisterGetters["oppervlakte"] = GettersOppervlakte
	RegisterGroupBy["oppervlakte"] = GettersOppervlakte

	//register filters for Lidid
	RegisterFuncMap["match-lidid"] = FilterLididMatch
	RegisterFuncMap["contains-lidid"] = FilterLididContains
	RegisterFuncMap["startswith-lidid"] = FilterLididStartsWith
	RegisterGetters["lidid"] = GettersLidid
	RegisterGroupBy["lidid"] = GettersLidid

	//register filters for MatchScore
	RegisterFuncMap["match-match_score"] = FilterMatchScoreMatch
	RegisterFuncMap["contains-match_score"] = FilterMatchScoreContains
	RegisterFuncMap["startswith-match_score"] = FilterMatchScoreStartsWith
	RegisterGetters["match_score"] = GettersMatchScore
	RegisterGroupBy["match_score"] = GettersMatchScore

	//register filters for BagToevoeging
	RegisterFuncMap["match-bag_toevoeging"] = FilterBagToevoegingMatch
	RegisterFuncMap["contains-bag_toevoeging"] = FilterBagToevoegingContains
	RegisterFuncMap["startswith-bag_toevoeging"] = FilterBagToevoegingStartsWith
	RegisterGetters["bag_toevoeging"] = GettersBagToevoeging
	RegisterGroupBy["bag_toevoeging"] = GettersBagToevoeging

	//register filters for Slug
	RegisterFuncMap["match-slug"] = FilterSlugMatch
	RegisterFuncMap["contains-slug"] = FilterSlugContains
	RegisterFuncMap["startswith-slug"] = FilterSlugStartsWith
	RegisterGetters["slug"] = GettersSlug
	RegisterGroupBy["slug"] = GettersSlug

	//register filters for Url
	RegisterFuncMap["match-url"] = FilterUrlMatch
	RegisterFuncMap["contains-url"] = FilterUrlContains
	RegisterFuncMap["startswith-url"] = FilterUrlStartsWith
	RegisterGetters["url"] = GettersUrl
	RegisterGroupBy["url"] = GettersUrl

	//register filters for Name
	RegisterFuncMap["match-name"] = FilterNameMatch
	RegisterFuncMap["contains-name"] = FilterNameContains
	RegisterFuncMap["startswith-name"] = FilterNameStartsWith
	RegisterGetters["name"] = GettersName
	RegisterGroupBy["name"] = GettersName

	//register filters for BovagPlaats
	RegisterFuncMap["match-bovag_plaats"] = FilterBovagPlaatsMatch
	RegisterFuncMap["contains-bovag_plaats"] = FilterBovagPlaatsContains
	RegisterFuncMap["startswith-bovag_plaats"] = FilterBovagPlaatsStartsWith
	RegisterGetters["bovag_plaats"] = GettersBovagPlaats
	RegisterGroupBy["bovag_plaats"] = GettersBovagPlaats

	//register filters for BovagPostcode
	RegisterFuncMap["match-bovag_postcode"] = FilterBovagPostcodeMatch
	RegisterFuncMap["contains-bovag_postcode"] = FilterBovagPostcodeContains
	RegisterFuncMap["startswith-bovag_postcode"] = FilterBovagPostcodeStartsWith
	RegisterGetters["bovag_postcode"] = GettersBovagPostcode
	RegisterGroupBy["bovag_postcode"] = GettersBovagPostcode

	//register filters for BovagAdres
	RegisterFuncMap["match-bovag_adres"] = FilterBovagAdresMatch
	RegisterFuncMap["contains-bovag_adres"] = FilterBovagAdresContains
	RegisterFuncMap["startswith-bovag_adres"] = FilterBovagAdresStartsWith
	RegisterGetters["bovag_adres"] = GettersBovagAdres
	RegisterGroupBy["bovag_adres"] = GettersBovagAdres

	//register filters for Geometry
	RegisterFuncMap["match-geometry"] = FilterGeometryMatch
	RegisterFuncMap["contains-geometry"] = FilterGeometryContains
	RegisterFuncMap["startswith-geometry"] = FilterGeometryStartsWith
	RegisterGetters["geometry"] = GettersGeometry
	RegisterGroupBy["geometry"] = GettersGeometry

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount
}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"pid":  func(i, j int) bool { return items[i].Pid < items[j].Pid },
		"-pid": func(i, j int) bool { return items[i].Pid > items[j].Pid },

		"vid":  func(i, j int) bool { return items[i].Vid < items[j].Vid },
		"-vid": func(i, j int) bool { return items[i].Vid > items[j].Vid },

		"sid":  func(i, j int) bool { return items[i].Sid < items[j].Sid },
		"-sid": func(i, j int) bool { return items[i].Sid > items[j].Sid },

		"lid":  func(i, j int) bool { return items[i].Lid < items[j].Lid },
		"-lid": func(i, j int) bool { return items[i].Lid > items[j].Lid },

		"numid":  func(i, j int) bool { return items[i].Numid < items[j].Numid },
		"-numid": func(i, j int) bool { return items[i].Numid > items[j].Numid },

		"huisnummer":  func(i, j int) bool { return items[i].Huisnummer < items[j].Huisnummer },
		"-huisnummer": func(i, j int) bool { return items[i].Huisnummer > items[j].Huisnummer },

		"huisletter":  func(i, j int) bool { return items[i].Huisletter < items[j].Huisletter },
		"-huisletter": func(i, j int) bool { return items[i].Huisletter > items[j].Huisletter },

		"huisnummertoevoeging":  func(i, j int) bool { return items[i].Huisnummertoevoeging < items[j].Huisnummertoevoeging },
		"-huisnummertoevoeging": func(i, j int) bool { return items[i].Huisnummertoevoeging > items[j].Huisnummertoevoeging },

		"postcode":  func(i, j int) bool { return items[i].Postcode < items[j].Postcode },
		"-postcode": func(i, j int) bool { return items[i].Postcode > items[j].Postcode },

		"oppervlakte":  func(i, j int) bool { return items[i].Oppervlakte < items[j].Oppervlakte },
		"-oppervlakte": func(i, j int) bool { return items[i].Oppervlakte > items[j].Oppervlakte },

		"lidid":  func(i, j int) bool { return items[i].Lidid < items[j].Lidid },
		"-lidid": func(i, j int) bool { return items[i].Lidid > items[j].Lidid },

		"match_score":  func(i, j int) bool { return items[i].MatchScore < items[j].MatchScore },
		"-match_score": func(i, j int) bool { return items[i].MatchScore > items[j].MatchScore },

		"bag_toevoeging":  func(i, j int) bool { return items[i].BagToevoeging < items[j].BagToevoeging },
		"-bag_toevoeging": func(i, j int) bool { return items[i].BagToevoeging > items[j].BagToevoeging },

		"slug":  func(i, j int) bool { return items[i].Slug < items[j].Slug },
		"-slug": func(i, j int) bool { return items[i].Slug > items[j].Slug },

		"url":  func(i, j int) bool { return items[i].Url < items[j].Url },
		"-url": func(i, j int) bool { return items[i].Url > items[j].Url },

		"name":  func(i, j int) bool { return items[i].Name < items[j].Name },
		"-name": func(i, j int) bool { return items[i].Name > items[j].Name },

		"bovag_plaats":  func(i, j int) bool { return items[i].BovagPlaats < items[j].BovagPlaats },
		"-bovag_plaats": func(i, j int) bool { return items[i].BovagPlaats > items[j].BovagPlaats },

		"bovag_postcode":  func(i, j int) bool { return items[i].BovagPostcode < items[j].BovagPostcode },
		"-bovag_postcode": func(i, j int) bool { return items[i].BovagPostcode > items[j].BovagPostcode },

		"bovag_adres":  func(i, j int) bool { return items[i].BovagAdres < items[j].BovagAdres },
		"-bovag_adres": func(i, j int) bool { return items[i].BovagAdres > items[j].BovagAdres },

		"geometry":  func(i, j int) bool { return items[i].Geometry < items[j].Geometry },
		"-geometry": func(i, j int) bool { return items[i].Geometry > items[j].Geometry },
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
